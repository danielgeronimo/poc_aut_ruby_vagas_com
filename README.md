*Automação Site Vagas**

## Tecnologias

* BDD
* Ruby
* Capybara
* SitePrism - PageObjects
* Bitbuckt - Repostório

## Explicação
* Autoamção Site Vagas: Foi automatizado, login, alteração de dados pessoais e endereço.

* Essa automação foi utilizada a tecnologias, BDD (Cucumber), Ruby, Capybara, SitePrism - PageObjects, Bitbuckt - Repostório.

* Na automação foi realizada com boas práticas com (hooks, log(Screenshot e  report em html) e cucumber.yaml)

* Poderia ser realizado a automação dos menus (Informações de Contato, Redes Sociais e Deficiência), porém à alteração de dados pessoais e endereço contém todos os tipos de elementos para automação (Combo, Text e Radio).

## Execução
* Ter Ruby e Git instalados na maquina
* Ter o driver do chrome configurado na maquina
* Criar uma pasta
* Dentro da pasta realizar o Clone: git clone https://danielgeronimo@bitbucket.org/danielgeronimo/poc_aut_ruby_vagas_com.git
* Acessar a pasta via  terminal e Executar o comando bundle install (no arquibo gemfile já estão todas as gem necessários e suas respectivas versões)
* Digitar apenas o comando cucumber (arquivo de conf yaml) está configurado para gerar os reports em htlm
* Sera gerado um report em html em features.html, abrir ele no browser (as evidências já estão integradas ao report).