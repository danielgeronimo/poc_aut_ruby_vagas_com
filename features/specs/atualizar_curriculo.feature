# language: pt

@atualizar_curriculo
Funcionalidade: Atualizar Currículo
  Para que eu possa atualizar meu currículo

Contexto: Conexto onde sempre será realizado o login para os cenários abaixo
  Dado que tenha usuário logado no site do vagas
  E clico em atualizar currículo
  E clico no menu dados pessoais

@dados_pessoais
Cenario: Atualizar dados pessoais do currículo 
  Quando eu clicar em editar dados pessoais
  E alterar os campos dados pessoais
  Então clico em salvar e exibe a mensagem "Dados salvos com sucesso."

@endereco
Cenario: Atualizar endereço do currículo 
  Quando eu clicar em editar endereço
  E alterar os dados do endereço
  Então clico em salvar e exibe a mensagem "Dados salvos com sucesso."