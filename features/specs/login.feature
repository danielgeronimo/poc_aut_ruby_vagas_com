# language: pt
@login
Funcionalidade: Realizar login no sie do vagas
  Como um candidato gostaria de realizar o login no site do vagas

Cenario: Realizar Login no site do vagas 
  Dado que tenha usuário "testeVagascom" e senha "Teste123456789"
  Quando eu realizado o login preenchendo os campos de usuário e senha
  E clico no botão para realizar o login
  Então exibo o nome do usuário na área logada