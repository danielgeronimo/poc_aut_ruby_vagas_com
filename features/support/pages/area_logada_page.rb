class AreaLogada < SitePrism::Page
  element :user_logado, '#topo-candidato-logado > .container > .components-holder > .header-components >#usuario-logado'
  element :btn_atualizar_curriculo, '#lateral-servicos > div > div.progresso > a'
end