class LoginPage < SitePrism::Page
  set_url '/login-candidatos'

  element :candidato, '#login_candidatos_form_usuario'
  element :senha, '#login_candidatos_form_senha'
  element :btn_entrar, 'input[name=commit]'

  def realizar_login(user, pwd)
    candidato.set user
    senha.set pwd
  end
end