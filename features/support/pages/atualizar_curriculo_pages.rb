class AtualizarCvPage < SitePrism::Page
  #elementos dos menus
  element :menu_dados_pessoais, '#dados'
  element :editar_dados_pessoais, '#cv-dados > a'
  element :editar_endereco, '#cv-endereco > a'
  element :editar_contato, '#informacoes-de-contato > a'
  element :editar_redes_sociais, '#redes > a'
  #Elementos dados pessoais
  element :dtn_nasc, '#dados_pessoais_data_de_nascimento'
  element :estado_civil, '#dados_pessoais_estado_civil'
  element :genero_masculino, '#dados_pessoais_genero_masculino'
  element :genero_feminino, '#dados_pessoais_genero_feminino'
  element :numero_cpf, '#dados_pessoais_documentos_attributes_0_numero'
  #Elementos endereço
  element :pais_endereco, '#endereco_pais_id'
  element :endereco_cep, '#endereco_cep'
  element :endereco_uf_id, '#endereco_uf_id'
  element :endereco_cidade_id, '#endereco_cidade_id'
  element :endereco_bairro, '#endereco_bairro'
  element :endereco_logradouro, '#endereco_logradouro'
  #Elemento generico para salvar
  element :btn_salvar, '.btnSaveBox'

  def atualizar_dados_pessoais(cpf, dtn)
    dtn_nasc.set dtn
    if genero_masculino.selected?
      genero_feminino.click
    else
      genero_masculino.click
    end

    if estado_civil.value == '1'
      estado_civil.find(:xpath, 'option[5]').select_option
    elsif estado_civil.value == '2'
      estado_civil.find(:xpath, 'option[4]').select_option
    elsif estado_civil.value == '3'
      estado_civil.find(:xpath, 'option[3]').select_option
    elsif estado_civil.value == '4'
      estado_civil.find(:xpath, 'option[2]').select_option
    elsif estado_civil.value == '5'
      estado_civil.find(:xpath, 'option[1]').select_option
    end
    numero_cpf.set cpf
  end

  def atualizar_endereco(endereco)
    endereco_cep.set endereco.first
    if pais_endereco.find('option', text: 'Brasil').selected? == true
      pais_endereco.find('option', text: 'Argentina').select_option
      endereco_uf_id.find('option', text: 'Buenos Aires').select_option
      endereco_cidade_id.find('option', text: 'Abel').select_option
    else
      pais_endereco.find('option', text: 'Brasil').select_option
      endereco_uf_id.find('option', text: 'São Paulo').select_option
      endereco_cidade_id.find('option', text: 'Sorocaba').select_option
    end

    if has_field?('endereco_bairro') == false
      btn_salvar.click
      sleep 2
    end
    endereco_bairro.set endereco[1]
    endereco_logradouro.set endereco.last
  end
end
