require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'cucumber'
require 'faker'
require 'cpf_faker'
require 'site_prism'
require 'pry'

Capybara.configure do |config|
  config.default_driver = :selenium_chrome
  config.app_host = "https://www.vagas.com.br"
end

Capybara.default_max_wait_time = 15.to_i
