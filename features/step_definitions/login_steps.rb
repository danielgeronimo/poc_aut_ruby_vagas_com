Dado(/^que tenha usuário "([^"]*)" e senha "([^"]*)"$/) do |usuario, senha|
  @user = usuario
  @pwd = senha
end

Quando(/^eu realizado o login preenchendo os campos de usuário e senha$/) do
  @login.load
  @login.realizar_login(@user,@pwd)
end

Quando(/^clico no botão para realizar o login$/) do
  @login.btn_entrar.click
end