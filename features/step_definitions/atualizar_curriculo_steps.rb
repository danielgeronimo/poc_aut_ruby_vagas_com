Dado(/^que tenha usuário logado no site do vagas$/) do
  step 'que tenha usuário "testeVagascom" e senha "Teste123456789"'
  step 'eu realizado o login preenchendo os campos de usuário e senha'
  step 'clico no botão para realizar o login'
  step 'exibo o nome do usuário na área logada'
end

Dado(/^clico no menu dados pessoais$/) do
  @atualizarpage.menu_dados_pessoais.click
end

Quando(/^eu clicar em editar dados pessoais$/) do
  @atualizarpage.editar_dados_pessoais.click
end

Quando(/^alterar os campos dados pessoais$/) do
  documento = Faker::CPF.numeric
  data_nascimento = Faker::Date.birthday(18, 65).strftime("%d/%m/%Y")
  sleep 1
  @atualizarpage.atualizar_dados_pessoais(documento, data_nascimento)
end

Quando(/^eu clicar em editar endereço$/) do
  @atualizarpage.editar_endereco.click
end

Quando(/^alterar os dados do endereço$/) do
  endereco = [cep = Faker::Number.number(8), bairro = Faker::StarWars.planet, ende = Faker::StarWars.droid]
  sleep 1
  @atualizarpage.atualizar_endereco(endereco)
end

Então(/^clico em salvar e exibe a mensagem "([^"]*)"$/) do |mensagem|
  @atualizarpage.btn_salvar.click
  sleep 1
  expect(page).to have_content mensagem
end
